%{?nodejs_find_provides_and_requires}
%global enable_tests 1
Name:                nodejs-eyes
Version:             0.1.8
Release:             2
Summary:             A customizable value inspector for Node.js
License:             MIT
URL:                 https://github.com/cloudhead/eyes.js
Source0:             https://github.com/cloudhead/eyes.js/archive/v%{version}.tar.gz
Patch0001:           nodejs-eyes-node12.patch
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
ExclusiveArch:       %{ix86} x86_64 %{arm} noarch
BuildRequires:       nodejs-packaging
%description
%{summary}

%prep
%autosetup -n eyes.js-%{version} -p1

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/eyes
cp -pr package.json lib/ \
    %{buildroot}%{nodejs_sitelib}/eyes
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%__nodejs test/*-test.js
%endif

%files
%doc LICENSE README.md
%{nodejs_sitelib}/eyes

%changelog
* Fri Nov 20 2020 lingsheng <lingsheng@huawei.com> - 0.1.8-2
- Fix tests for node 12.x support

* Thu Aug 20 2020 Anan Fu <fuanan3@huawei.com> - 0.1.8-1
- package init
